cd `dirname $0`

rm -fR web_production
GOOS=linux GOARCH=amd64 $GOPATH/bin/revel build bitbucket.org/bioservices/echinodb/goweb web_production
rsync -avzP web_production/ bioservices.uncc.edu:/srv/www/uwsgi/echinodb-go/

