package controllers

import (
	"database/sql"
	"fmt"
	"github.com/lib/pq/hstore"
	"regexp"
	"strconv"
	"strings"
)

// SpeciesTree maps CLASS, ORDER, FAMILY, GENUS, SPECIES => bjnumber
type SpeciesTree map[string]map[string]map[string]map[string]map[string]int64

//////////////
type Set map[string]struct{}

func (s Set) Delete(v string) {
	delete(s, v)
}
func (s *Set) Put(v string) {
	if *s == nil {
		*s = make(Set)
	}
	(*s)[v] = struct{}{}
}
func (s Set) Contains(v string) bool {
	if s == nil {
		return false
	}
	_, has := s[v]
	return has
}
func (s *Set) PutInt(v int64) {
	if *s == nil {
		*s = make(Set)
	}
	(*s)[strconv.FormatInt(v, 10)] = struct{}{}
}
func (s Set) ContainsInt(v int64) bool {
	if s == nil {
		return false
	}
	_, has := s[strconv.FormatInt(v, 10)]
	return has
}

//////////////

// FixDNAFromPeptide given a peptide string and corresponding nucleotide string,
// determine if the nucleotide sequence should be reversed and/or complemented,
// and return the matching sequence.
func FixDNAFromPeptide(aminos, nucleotides string, didReverse ...bool) (string, bool) {
	comp := map[byte]byte{
		'A': 'T',
		'T': 'A',
		'G': 'C',
		'C': 'G',
	}

	justComp := 0
	mismatches := 0
	for i := 0; i < 5; i++ {
		codon := nucleotides[i*3 : (i+1)*3]
		aa := nuc2aa[codon]
		if aa != aminos[i:i+1] {
			mismatches++
		}
		bcodon := []byte(codon)
		bcodon[0] = comp[bcodon[0]]
		bcodon[1] = comp[bcodon[1]]
		bcodon[2] = comp[bcodon[2]]
		aa = nuc2aa[string(bcodon)]
		if aa == aminos[i:i+1] {
			justComp++
		}
	}

	if mismatches == 0 {
		// already matches
		return nucleotides, false
	}

	if justComp >= mismatches {
		// just take the complement and it'll match
		nsb := []byte(nucleotides)
		for i, b := range nsb {
			nsb[i] = comp[b]
		}
		return string(nsb), true
	}

	if len(didReverse) == 1 && didReverse[0] {
		return "sequence error!", false
	}

	nb := []byte(nucleotides)
	nb2 := make([]byte, len(nb))
	for i := 0; i < len(nb); i++ {
		nb2[len(nb)-1-i] = nb[i]
	}
	return FixDNAFromPeptide(aminos, string(nb2), true)
}

// GetSpeciesTree returns a phylo tree structure from Class...Species, along
// with a set of names within the tree
func GetSpeciesTree() (SpeciesTree, Set, error) {
	spc := make(SpeciesTree)
	var spNames Set

	rows, err := Db.Queryx("SELECT bjnumber,sp_class,sp_order,sp_family,sp_genus,sp_species FROM species;")
	if err != nil {
		return nil, nil, err
	}

	var bjnumber int64
	var spClass, spOrder, spFamily, spGenus, spSpecies string

	for rows.Next() {
		err = rows.Scan(&bjnumber, &spClass, &spOrder, &spFamily, &spGenus, &spSpecies)
		if err != nil {
			return nil, nil, err
		}

		if _, f := spc[spClass]; !f {
			spc[spClass] = make(map[string]map[string]map[string]map[string]int64)
		}
		if _, f := spc[spClass][spOrder]; !f {
			spc[spClass][spOrder] = make(map[string]map[string]map[string]int64)
		}
		if _, f := spc[spClass][spOrder][spFamily]; !f {
			spc[spClass][spOrder][spFamily] = make(map[string]map[string]int64)
		}
		if _, f := spc[spClass][spOrder][spFamily][spGenus]; !f {
			spc[spClass][spOrder][spFamily][spGenus] = make(map[string]int64)
		}
		if _, f := spc[spClass][spOrder][spFamily][spGenus]; !f {
			spc[spClass][spOrder][spFamily][spGenus] = make(map[string]int64)
		}
		spc[spClass][spOrder][spFamily][spGenus][spSpecies] = bjnumber
		spNames.Put(spClass)
		spNames.Put(spOrder)
		spNames.Put(spFamily)
		spNames.Put(spGenus)
		spNames.Put(spSpecies)
		spNames.PutInt(bjnumber)
	}

	return spc, spNames, nil
}

// GetSelectedFromTree returns a set of names matching bjnums within the tree
func GetSelectedFromTree(bjnums []int64, spc SpeciesTree) (map[int64]string, Set) {
	spNames := make(map[int64]string)
	var spSelected Set

	for spClass, orders := range spc {
		for spOrder, families := range orders {
			for spFamily, geni := range families {
				for spGenus, species := range geni {
					for spName, bjnumber := range species {
						for _, bj := range bjnums {
							if bj == bjnumber {
								spSelected.Put(spClass)
								spSelected.Put(spOrder)
								spSelected.Put(spFamily)
								spSelected.Put(spGenus)
								spSelected.Put(spName)
								spSelected.PutInt(bjnumber)
							}
						}
						spNames[bjnumber] = spGenus + " " + spName
					}
				}
			}
		}
	}

	return spNames, spSelected
}

type ResultRow struct {
	BjNumber        int64          `db:"bjnumber"`
	GiNumber        string         `db:"gi_num"`
	LocusTranscript string         `db:"locustranscript"`
	NucSequence     sql.NullString `db:"sequence"`
	Aanumber        sql.NullString `db:"aanumber"`
	PepSequence     sql.NullString `db:"peptide_sequence"`
	RefseqID        sql.NullString `db:"refseqid"`
	RefseqName      sql.NullString `db:"refseq_name"`
	RefSequence     sql.NullString `db:"refseq_sequence"`
	RefseqType      sql.NullString `db:"refseq_type"`
}

type ClusterResultRow struct {
	ClusterID   string        `db:"clusterid"`
	NumHits     int           `db:"num_hits"`
	GiNumber    string        `db:"gi_num"`
	RefseqName  string        `db:"rfname"`
	RefSequence string        `db:"refseq_sequence"`
	RefIDs      hstore.Hstore `db:"otherids"`
}

type SequenceResultRow struct {
	BjNumber      int64          `db:"bjnumber"`
	AaNumber      string         `db:"aanumber"`
	Sequence      string         `db:"sequence"`
	Start         int            `db:"startpos"`
	End           int            `db:"endpos"`
	ReadCount     sql.NullInt64  `db:"readcount"`
	OtherSequence sql.NullString `db:"other_sequence"`
}

func (r *SequenceResultRow) SeqLen(isAmino bool) int {
	if isAmino {
		return len(r.Sequence) * 3
	}
	return len(r.Sequence)
}

func (r *SequenceResultRow) ReadsPerKB(isAmino bool) int64 {
	n := int64(len(r.Sequence))
	if isAmino {
		n *= 3
	}
	return (r.ReadCount.Int64 * 1000) / n
}

func (c ClusterResultRow) GetRefID() string {
	keys := []string{"ref", "gb", "emb"}
	for _, k := range keys {
		if s, found := c.RefIDs.Map[k]; found {
			return s.String
		}
	}
	for _, v := range c.RefIDs.Map {
		return v.String
	}
	return c.GiNumber
}

func wrapSequence(seq string) (res []string) {
	i := 0
	if len(seq) > 80 {
		for ; i < len(seq)-80; i += 80 {
			res = append(res, seq[i:i+80])
		}
	}
	res = append(res, seq[i:])
	return res
}

func GetFastaCluster(clusterid, seqtype string, bjnumstr string, speciesLabels map[int64]string) ([]string, error) {
	bjsql := ""
	if bjnumstr != "" {
		bjsql = "AND o.bjnumber=ANY('{" + bjnumstr + "}')"
	}
	SQL := `SELECT o.clusterid, COUNT(o.aanumber) as num_hits, r.gi_num,
                 r.rfname, r.amino_sequence as refseq_sequence, r.otherids
            FROM orthoclusters o, refseqs r
           WHERE o.gi_num=r.gi_num AND o.clusterid=$1 ` + bjsql + `
           GROUP BY o.clusterid, r.gi_num, r.rfname, refseq_sequence, r.otherids`

	cluster := ClusterResultRow{}
	err := Db.Get(&cluster, SQL, clusterid)
	if err != nil {
		return nil, err
	}

	results := []string{}
	if seqtype == "peptide" {
		results = append(results, fmt.Sprintf("> RefSeq %v (%v) %v", cluster.GetRefID(), cluster.GiNumber, cluster.RefseqName))
		results = append(results, wrapSequence(cluster.RefSequence)...)

		aminos := []SequenceResultRow{}
		err = Db.Select(&aminos, `select a.bjnumber, a.aanumber, amino_sequence as sequence
     from amino a, orthoclusters o WHERE a.bjnumber=o.bjnumber and a.aanumber=o.aanumber
     and o.clusterid=$1 `+bjsql+`;`, clusterid)
		if err != nil {
			return nil, err
		}
		for _, amino := range aminos {
			results = append(results, fmt.Sprintf("> %v-bj%v aa%v [%v]", clusterid, amino.BjNumber, amino.AaNumber, speciesLabels[amino.BjNumber]))
			results = append(results, wrapSequence(amino.Sequence)...)
		}

	} else {
		nucleos := []SequenceResultRow{}
		err = Db.Select(&nucleos, `select a.bjnumber, n.locustranscript as aanumber,
			              nuc_sequence as sequence, startpos, endpos, amino_sequence as other_sequence
               from nucleotide2amino a, nucleotide n, orthoclusters o, amino aa
              WHERE a.bjnumber=o.bjnumber and a.aanumber=o.aanumber
                and a.aanumber=aa.aanumber and a.bjnumber=aa.bjnumber
                and n.bjnumber=a.bjnumber and a.locustranscript=n.locustranscript
                and o.clusterid=$1 `+bjsql+`;`, clusterid)
		if err != nil {
			return nil, err
		}

		if seqtype == "cds" {
			for _, nucleo := range nucleos {

				nseq := ""
				if nucleo.Start > nucleo.End {
					runes := []rune(nucleo.Sequence[nucleo.End-1 : nucleo.Start])
					for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
						runes[i], runes[j] = runes[j], runes[i]
					}
					nseq = string(runes)
				} else {
					e := nucleo.End
					if e > len(nucleo.Sequence) {
						e = len(nucleo.Sequence)
					}
					nseq = nucleo.Sequence[nucleo.Start-1 : e]
				}

				didRev := ""
				nseq, didComp := FixDNAFromPeptide(nucleo.OtherSequence.String, nseq)
				if didComp {
					didRev = "!"
				}

				results = append(results, fmt.Sprintf("> %v-bj%v %v:(%v-%v)%s [%v]",
					clusterid, nucleo.BjNumber, nucleo.AaNumber, nucleo.Start, nucleo.End,
					didRev, speciesLabels[nucleo.BjNumber]))
				results = append(results, wrapSequence(nseq)...)
			}
		} else {
			for _, nucleo := range nucleos {
				results = append(results, fmt.Sprintf("> %v-bj%v %v [%v]", clusterid, nucleo.BjNumber, nucleo.AaNumber, speciesLabels[nucleo.BjNumber]))
				results = append(results, wrapSequence(nucleo.Sequence)...)
			}
		}
	}
	return results, nil
}

type CustomClusterRow struct {
	BjNumber int
	AANumber int
}
type CustomCluster []*CustomClusterRow

func NewCustomCluster(spec string) (CustomCluster, error) {
	if strings.Contains(spec, "\n") {
		spec = strings.Replace(spec, "\r\n", ",", -1)
		spec = strings.Replace(spec, "\n", ",", -1)
	}
	nonumer := regexp.MustCompile("[^0-9-]")
	manydash := regexp.MustCompile("-{2,}")
	clus := CustomCluster{}
	for _, member := range strings.Split(spec, ",") {
		member = nonumer.ReplaceAllString(member, "-")
		member = manydash.ReplaceAllString(member, "-")
		member = strings.Trim(member, "-")
		row := &CustomClusterRow{}
		if _, err := fmt.Sscanf(member, "%d-%d", &row.BjNumber, &row.AANumber); err != nil {
			return nil, err
		}
		clus = append(clus, row)
	}
	return clus, nil
}

func (c *CustomClusterRow) String() string {
	return fmt.Sprintf("%d-%d", c.BjNumber, c.AANumber)
}

func (c CustomCluster) String() string {
	res := make([]string, len(c))
	for i, member := range c {
		res[i] = member.String()
	}
	return strings.Join(res, ",")
}

func GetFastaExport(clus CustomCluster, seqtype string, speciesLabels map[int64]string) (results []string, err error) {
	if seqtype == "peptide" {
		aminos := []SequenceResultRow{}
		err = Db.Select(&aminos, `select a.bjnumber, a.aanumber, amino_sequence as sequence
     			from amino a WHERE a.bjnumber||'-'||a.aanumber=ANY($1);`, "{"+clus.String()+"}")
		if err != nil {
			return nil, err
		}
		for _, amino := range aminos {
			results = append(results, fmt.Sprintf("> bj%v.aa%v [%v]", amino.BjNumber, amino.AaNumber, speciesLabels[amino.BjNumber]))
			results = append(results, wrapSequence(amino.Sequence)...)
		}

	} else {
		nucleos := []SequenceResultRow{}
		err = Db.Select(&nucleos, `select a.bjnumber, a.aanumber||' '||n.locustranscript as aanumber,
									  aa.amino_sequence as other_sequence, nuc_sequence as sequence, startpos, endpos
               from amino aa, nucleotide2amino a, nucleotide n
              where n.bjnumber=a.bjnumber and a.locustranscript=n.locustranscript
              	and a.aanumber=aa.aanumber and a.bjnumber=aa.bjnumber
                and a.bjnumber||'-'||a.aanumber=ANY($1);`, "{"+clus.String()+"}")
		if err != nil {
			return nil, err
		}

		if seqtype == "cds" {
			for _, nucleo := range nucleos {

				nseq := ""
				if nucleo.Start > nucleo.End {
					runes := []rune(nucleo.Sequence[nucleo.End-1 : nucleo.Start])
					for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
						runes[i], runes[j] = runes[j], runes[i]
					}
					nseq = string(runes)
				} else {
					e := nucleo.End
					if e > len(nucleo.Sequence) {
						e = len(nucleo.Sequence)
					}
					nseq = nucleo.Sequence[nucleo.Start-1 : e]
				}

				didRev := ""
				nseq, didComp := FixDNAFromPeptide(nucleo.OtherSequence.String, nseq)
				if didComp {
					didRev = "!"
				}

				results = append(results, fmt.Sprintf("> bj%v.aa%v:(%v-%v)%s [%v]",
					nucleo.BjNumber, nucleo.AaNumber, nucleo.Start, nucleo.End, didRev, speciesLabels[nucleo.BjNumber]))
				results = append(results, wrapSequence(nseq)...)
			}
		} else {
			for _, nucleo := range nucleos {
				results = append(results, fmt.Sprintf("> bj%v.aa%v [%v]", nucleo.BjNumber, nucleo.AaNumber, speciesLabels[nucleo.BjNumber]))
				results = append(results, wrapSequence(nucleo.Sequence)...)
			}
		}
	}
	return results, nil
}

var (
	nuc2aa = make(map[string]string)
	aa2nuc = make(map[string]string)
)

func init() {
	aminos := []byte("FFLLSSSSYY**CC*WLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG")

	// Bases
	bases := [][]byte{
		[]byte("TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG"),
		[]byte("TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG"),
		[]byte("TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG"),
	}

	for i, aa := range aminos {
		nuc := []byte{bases[0][i], bases[1][i], bases[2][i]}
		nuc2aa[string(nuc)] = string(aa)
		aa2nuc[string(aa)] = string(nuc)
	}
}
