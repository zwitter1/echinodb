package controllers

import (
	"archive/zip"
	"bitbucket.org/bioservices/echinodb/goweb/app/routes"
	"bytes"
	"github.com/revel/revel"
	"strconv"
	"strings"
	"time"
)

type App struct {
	*revel.Controller
}

func (c App) Auth(pass string) revel.Result {
	if pass == revel.Config.StringDefault("echinodb.httppass", "password") {
		c.Session["auth"] = "ok"
		return c.Redirect(routes.App.Index())
	}
	delete(c.Session, "auth")

	html := `Password: <form action="/echinodb/auth" method="post"><input type="password" name="pass"><input type="submit"></form>`
	return c.RenderHtml(html)
}

func (c App) GetNAAlignment(bjnumber, aanumber int64) revel.Result {
	if _, authed := c.Session["auth"]; !authed {
		return c.Redirect(routes.App.Auth(""))
	}

	amino := SequenceResultRow{}
	err := Db.Get(&amino, `select bjnumber, aanumber, amino_sequence as sequence
     from amino WHERE bjnumber=$1 and aanumber=$2;`, bjnumber, aanumber)
	if err != nil {
		return c.RenderError(err)
	}

	nucleo := SequenceResultRow{}
	err = Db.Get(&nucleo, `select a.bjnumber, a.aanumber, nuc_sequence as sequence, startpos, endpos
     from nucleotide2amino a, nucleotide n WHERE n.bjnumber=a.bjnumber and a.locustranscript=n.locustranscript
      and a.bjnumber=$1 and a.aanumber=$2;`, bjnumber, aanumber)
	if err != nil {
		return c.RenderError(err)
	}

	nseq := ""
	if nucleo.Start > nucleo.End {
		ee := nucleo.Start
		if len(nucleo.Sequence) < ee {
			ee = len(nucleo.Sequence)
		}
		runes := []rune(nucleo.Sequence[nucleo.End-1 : ee])
		for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
			runes[i], runes[j] = runes[j], runes[i]
		}
		nseq = string(runes)
	} else {
		ee := nucleo.End
		if len(nucleo.Sequence) < ee {
			ee = len(nucleo.Sequence)
		}
		nseq = nucleo.Sequence[nucleo.Start-1 : ee]
	}
	lines := []string{}

	nseq, didComp := FixDNAFromPeptide(amino.Sequence, nseq)
	if didComp {
		lines = append(lines, "comped!")
	}

	aseq := ""
	alen := 0
	for _, c := range amino.Sequence {
		aseq += string(c) + "&#8209;&#8209;"
		alen += 3
		if alen >= 59 {
			lines = append(lines, nseq[:alen])
			lines = append(lines, aseq)
			nseq = nseq[alen:]
			aseq = ""
			alen = 0
		}
	}
	lines = append(lines, nseq)
	lines = append(lines, aseq)

	return c.RenderText(strings.Join(lines, "<br/>\n"))
}

func (c App) ViewCluster(clusterid string, bjnums string) revel.Result {
	if _, authed := c.Session["auth"]; !authed {
		return c.Redirect(routes.App.Auth(""))
	}
	speciesTree, _, err := GetSpeciesTree()
	if err != nil {
		return c.RenderError(err)
	}

	bjints := []int64{}
	chkAll := false
	bjnumstr := ""
	if len(bjnums) > 0 {
		// parse to protect from SQL injection...
		for _, bjs := range strings.Split(bjnums, ",") {
			if bjs == "" {
				continue
			}
			bjn, err := strconv.ParseInt(bjs, 10, 64)
			if err != nil {
				return c.RenderError(err)
			}
			bjints = append(bjints, bjn)
			bjnumstr += "," + bjs
		}
		bjnumstr = " AND o.bjnumber=ANY('{" + bjnumstr[1:] + "}')"
	} else {
		chkAll = true
	}
	prettyNames, chkNames := GetSelectedFromTree(bjints, speciesTree)

	results := []SequenceResultRow{}
	err = Db.Select(&results, `select a.bjnumber, a.aanumber, amino_sequence as sequence, n.readcount
     from amino a, orthoclusters o, nucleotide2amino na, nucleotide n
    where o.bjnumber=a.bjnumber and o.aanumber=a.aanumber and na.bjnumber=a.bjnumber
      and na.aanumber=a.aanumber and na.locustranscript=n.locustranscript and na.bjnumber=n.bjnumber
      and o.clusterid=$1 `+bjnumstr+`;`, clusterid)
	if err != nil {
		return c.RenderError(err)
	}

	SQL := `SELECT o.clusterid, COUNT(o.aanumber) as num_hits, r.gi_num,
                 r.rfname, r.amino_sequence as refseq_sequence, r.otherids
            FROM orthoclusters o, refseqs r
           WHERE o.gi_num=r.gi_num AND o.clusterid=$1 ` + bjnumstr + `
           GROUP BY o.clusterid, r.gi_num, r.rfname, refseq_sequence, r.otherids`

	cluster := ClusterResultRow{}
	err = Db.Get(&cluster, SQL, clusterid)

	return c.Render(results, speciesTree, prettyNames, cluster, bjnums, chkNames, chkAll)
}

func (c App) MakeCluster() revel.Result {
	if _, authed := c.Session["auth"]; !authed {
		return c.Redirect(routes.App.Auth(""))
	}
	return c.Render()
}

func (c App) MakeClusterPost(q string, export string) revel.Result {
	if _, authed := c.Session["auth"]; !authed {
		return c.Redirect(routes.App.Auth(""))
	}

	speciesTree, _, err := GetSpeciesTree()
	if err != nil {
		return c.RenderError(err)
	}
	prettyNames, _ := GetSelectedFromTree(nil, speciesTree)

	clus, err := NewCustomCluster(q)
	if err != nil {
		return c.RenderError(err)
	}

	results, err := GetFastaExport(clus, strings.ToLower(export), prettyNames)
	if err != nil {
		return c.RenderError(err)
	}

	filename := "custom.fna"
	if strings.ToLower(export) == "peptide" {
		filename = "custom.faa"
	}
	buf := bytes.NewBufferString(strings.Join(results, "\n"))
	return c.RenderBinary(buf, filename, revel.Attachment, time.Now())
}

func (c App) Index() revel.Result {
	if _, authed := c.Session["auth"]; !authed {
		return c.Redirect(routes.App.Auth(""))
	}
	speciesTree, phyloNames, err := GetSpeciesTree()
	if err != nil {
		return c.RenderError(err)
	}
	chkAll := true
	chkNames := phyloNames
	return c.Render(speciesTree, phyloNames, chkNames, chkAll)
}

func (c App) Search(q string, bjnums []int64, allspecies string) revel.Result {
	if _, authed := c.Session["auth"]; !authed {
		return c.Redirect(routes.App.Auth(""))
	}
	speciesTree, phyloNames, err := GetSpeciesTree()
	if err != nil {
		return c.RenderError(err)
	}
	prettyNames, chkNames := GetSelectedFromTree(bjnums, speciesTree)

	SQLWHERE := "r._searchtext @@ plainto_tsquery($1)"
	qparams := []interface{}{strings.ToLower(q)}

	if strings.Contains(q, "*") {
		qp := strings.Split(strings.Replace(strings.ToLower(q), "*", ":*", -1), " ")
		q = strings.Join(qp, " & ")
		SQLWHERE = "r._searchtext @@ to_tsquery($1)"
		qparams = []interface{}{q}
	}

	qbjnums := ""
	chkAll := false
	if allspecies == "all" {
		chkAll = true
		chkNames = phyloNames
	} else {
		SQLWHERE += " AND o.bjnumber=ANY($2)"

		bjarray := ""
		for i, bj := range bjnums {
			if i != 0 {
				bjarray += "," + strconv.FormatInt(bj, 10)
			} else {
				bjarray += strconv.FormatInt(bj, 10)
			}
		}

		qbjnums = "?bjnums=" + bjarray
		bjarray = "{" + bjarray + "}"

		qparams = append(qparams, bjarray)
	}

	SQL := `SELECT o.clusterid, COUNT(o.aanumber) as num_hits, r.gi_num,
                 r.rfname, r.amino_sequence as refseq_sequence, r.otherids
            FROM orthoclusters o, refseqs r
           WHERE o.gi_num=r.gi_num AND ` + SQLWHERE + `
           GROUP BY o.clusterid, r.gi_num, r.rfname, refseq_sequence, r.otherids`

	results := []*ClusterResultRow{}
	err = Db.Select(&results, SQL+" LIMIT 1000;", qparams...)

	if err != nil {
		return c.RenderError(err)
	}

	return c.Render(q, qbjnums, speciesTree, phyloNames, prettyNames, chkNames, chkAll, results)
}

func (c App) ExportCluster(clusterid, export string, bjnums string) revel.Result {
	if _, authed := c.Session["auth"]; !authed {
		return c.Redirect(routes.App.Auth(""))
	}

	speciesTree, _, err := GetSpeciesTree()
	if err != nil {
		return c.RenderError(err)
	}
	prettyNames, _ := GetSelectedFromTree(nil, speciesTree)

	if len(bjnums) > 0 {
		bjnumstr := ""
		// sanitize input / protect from SQL injection
		for _, bjs := range strings.Split(bjnums, ",") {
			if bjs == "" {
				continue
			}
			_, err := strconv.ParseInt(bjs, 10, 64)
			if err != nil {
				return c.RenderError(err)
			}
			bjnumstr += "," + bjs
		}
		bjnums = bjnumstr[1:]
	}

	if export != "zip" {
		results, err := GetFastaCluster(clusterid, strings.ToLower(export), bjnums, prettyNames)
		if err != nil {
			return c.RenderError(err)
		}

		filename := clusterid + ".fna"
		if strings.ToLower(export) == "peptide" {
			filename = clusterid + ".faa"
		}
		buf := bytes.NewBufferString(strings.Join(results, "\n"))
		return c.RenderBinary(buf, filename, revel.Attachment, time.Now())
	}

	nuc_results, err := GetFastaCluster(clusterid, "nucleotide", bjnums, prettyNames)
	if err != nil {
		return c.RenderError(err)
	}
	cds_results, err := GetFastaCluster(clusterid, "cds", bjnums, prettyNames)
	if err != nil {
		return c.RenderError(err)
	}
	pep_results, err := GetFastaCluster(clusterid, "peptide", bjnums, prettyNames)
	if err != nil {
		return c.RenderError(err)
	}

	// Create a buffer to write our archive to.
	buf := new(bytes.Buffer)

	// Create a new zip archive.
	w := zip.NewWriter(buf)

	// Add some files to the archive.
	var files = []struct {
		Name, Body string
	}{
		{clusterid + ".fna", strings.Join(nuc_results, "\n")},
		{clusterid + ".cds.fna", strings.Join(cds_results, "\n")},
		{clusterid + ".faa", strings.Join(pep_results, "\n")},
	}
	for _, file := range files {
		f, err := w.Create(file.Name)
		if err != nil {
			return c.RenderError(err)
		}
		_, err = f.Write([]byte(file.Body))
		if err != nil {
			return c.RenderError(err)
		}
	}

	// Make sure to check the error on Close.
	err = w.Close()
	if err != nil {
		return c.RenderError(err)
	}

	return c.RenderBinary(buf, clusterid+".zip", revel.Attachment, time.Now())
}

func (c App) About() revel.Result {
	if _, authed := c.Session["auth"]; !authed {
		return c.Redirect(routes.App.Auth(""))
	}
	return c.Render()
}
