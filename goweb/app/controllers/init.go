package controllers

import (
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/revel/revel"
)

var Db *sqlx.DB

func InitDB() {
	var err error
	spec, hasSpec := revel.Config.String("echinodb.dbspec")
	if !hasSpec {
		panic("must define a db connection string in echinodb.dbspec!")
	}
	Db, err = sqlx.Connect("postgres", spec)
	if err != nil {
		panic(err)
	}
}
