import re
import json
from flask import Flask, session, request, render_template, flash, redirect, url_for, make_response

webdir = os.path.dirname(os.path.abspath(__file__))
configdata = json.loads(open(os.path.join(webdir, "config.json")).read())

app = Flask(__name__)
app.secret_key = configdata['COOKIE_SECRET']
app.config['TESTING']=True
app.config['DEBUG']=True

############################################################################
############################################################################
import psycopg2
from psycopg2.extras import RealDictCursor


def get_species_tree(dbcur):
  spc={}
  allbjs=set()
  dbcur.execute('SELECT * FROM species ORDER BY sp_class,sp_order,sp_family,sp_genus,sp_species;')
  for row in dbcur:
    if row['sp_class'] not in spc:
      spc[ row['sp_class'] ] = {}
    if row['sp_order'] not in spc[ row['sp_class'] ]:
      spc[ row['sp_class'] ][ row['sp_order'] ] = {}
    if row['sp_family'] not in spc[ row['sp_class'] ][ row['sp_order'] ]:
      spc[ row['sp_class'] ][ row['sp_order'] ][ row['sp_family'] ] = {}
    if row['sp_genus'] not in spc[ row['sp_class'] ][ row['sp_order'] ][ row['sp_family'] ]:
      spc[ row['sp_class'] ][ row['sp_order'] ][ row['sp_family'] ][ row['sp_genus'] ] = {}
    spc[ row['sp_class'] ][ row['sp_order'] ][ row['sp_family'] ][ row['sp_genus'] ][ row['sp_species'] ] = row['bjnumber']

    for key,val in row.items():
      if key[:3]=='sp_':
        allbjs.add(val)
    allbjs.add(row['bjnumber'])
  return spc, allbjs

@app.route('/echinodb/auth', methods=['GET','POST'])
def auth():
  if request.form.get('pass')==configdata['HTTP_PASSWORD']:
    session['authed']=True
    return redirect('/echinodb/')
  session['authed']=False
  return 'Password: <form action="" method="post"><input type="password" name="pass"><input type="submit"></form>'

@app.route('/echinodb/', methods=['GET'])
def main_index():
  if session.get('authed')!=True:
    return redirect('/echinodb/auth')
  db = psycopg2.connect(configdata['DB_CONNECT_STRING'])
  cur = db.cursor(cursor_factory=RealDictCursor)
  species, checkedbjs = get_species_tree(cur)
  return render_template('index.html', species=species, checkedbjs=checkedbjs, isallbjs=True, sorted=sorted)

@app.route('/echinodb/export', methods=['POST'])
def export_results():
  if session.get('authed')!=True:
    return redirect('/echinodb/auth')
  db = psycopg2.connect(configdata['DB_CONNECT_STRING'])
  cur = db.cursor()

  results = []
  export_type = request.form.get('export', 'peptide')
  if export_type=='peptide':
    for q in request.form.getlist('selected'):
      bjnum, ginum, lt = q.split(',',2)

      cur.execute("""SELECT a.aanumber, r.refseq_name, r.refseqid, a.amino_sequence FROM refseq r, amino a
                      WHERE r.gi_num=%s and a.bjnumber=%s AND a.aanumber IN (SELECT aanumber from nucleotide2amino WHERE bjnumber=%s AND locustranscript=%s)
                      ORDER BY LENGTH(amino_sequence) DESC LIMIT 1;""", (ginum,bjnum,bjnum,lt))
      row = cur.fetchone()
      refline = '> bj%s-aa%s hit on %s (%s)' % (bjnum, row[0], row[1], row[2])
      results.append( refline )
      results.extend( row[3][i:i+80] for i in range(0,len(row[3]),80) )

  else:
    for q in request.form.getlist('selected'):
      bjnum, ginum, lt = q.split(',',2)

      cur.execute("""SELECT r.refseq_name, r.refseqid, n.nuc_sequence FROM refseq r, nucleotide n
                      WHERE r.gi_num=%s and n.bjnumber=%s AND n.locustranscript=%s LIMIT 1;""", (ginum,bjnum,lt))
      row = cur.fetchone()
      refline = '> bj%s [%s] hit on %s (%s)' % (bjnum, lt, row[0], row[1])
      results.append( refline )
      results.extend( row[2][i:i+80] for i in range(0,len(row[2]),80) )

  resp = make_response("\n".join(results), 200)
  resp.headers['Content-Type'] = 'text/plain'
  return resp

@app.route('/echinodb/', methods=['POST'])
def search_results():
  if session.get('authed')!=True:
    return redirect('/echinodb/auth')
  db = psycopg2.connect(configdata['DB_CONNECT_STRING'])
  cur = db.cursor(cursor_factory=RealDictCursor)

  q = request.form.get('q')
  bjs = [int(x) for x in request.form.getlist('bjnum')]

  species, allbjs = get_species_tree(cur)

  checkedbjs=set()
  prettybjs={}
  cur.execute('SELECT * FROM species WHERE bjnumber=ANY(%s);', (bjs,))
  for row in cur:
    prettybjs[row['bjnumber']] = row['sp_genus']+' '+row['sp_species']
    if prettybjs[row['bjnumber']]=='? ?':
      prettybjs[row['bjnumber']] = 'unnamed '+row['sp_order']

    for key,val in row.items():
      if key[:3]=='sp_':
        checkedbjs.add(val)
    checkedbjs.add(row['bjnumber'])

  if request.form.get('allspecies')=='all':
    checkedbjs=allbjs

  qparams = [q.lower(), q.lower()]
  SQLWHERE = "LOWER(r.refseqid) LIKE '%%'||%s||'%%'"
  SQLWHERE += " OR LOWER(r.refseq_name) LIKE '%%'||%s||'%%'"
  if re.search('[A-Za-z]', q) is None:
    SQLWHERE += " OR r.gi_num=%s"
    qparams.append(q)

  cur.execute("""SELECT b.bjnumber, b.gi_num, 0.0 as evalue,
                          n.locustranscript, n.nuc_sequence as sequence,
                          a.aanumber, a.amino_sequence as peptide_sequence,

                          r.refseqid,
                          r.refseq_name,
                          r.refseq_sequence,
                          r.refseq_type

                   FROM flatblast b, refseq r, nucleotide n
                        LEFT OUTER JOIN nucleotide2amino na ON (n.locustranscript=na.locustranscript AND n.bjnumber=na.bjnumber)
                        LEFT OUTER JOIN amino a ON (na.aanumber=a.aanumber AND na.bjnumber=a.bjnumber)
                  WHERE b.bjnumber=n.bjnumber AND b.locustranscript=n.locustranscript AND b.gi_num=r.gi_num
                    AND ("""+SQLWHERE+""") AND b.bjnumber=ANY(%s);""", tuple(qparams+[bjs]))
  results = cur.fetchmany(1000)
  return render_template('results.html', species=species, isallbjs=(len(allbjs.difference(checkedbjs))==0), prettybjs=prettybjs, checkedbjs=checkedbjs, query=q,
                         results=results, sorted=sorted)

if __name__ == '__main__':
  app.config['SERVER_NAME']='skittles.local:5001'
  app.config['TESTING']=True
  app.config['DEBUG']=True
  app.run(host='0.0.0.0', port=5001)
