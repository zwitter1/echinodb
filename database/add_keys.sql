/* Create Foreign Keys */

ALTER TABLE nucleotide2amino
	ADD FOREIGN KEY (bjnumber, aanumber)
	REFERENCES amino (bjnumber, aanumber)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE nucleotide2amino
	ADD FOREIGN KEY (bjnumber, locustranscript)
	REFERENCES nucleotide (bjnumber, locustranscript)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;

ALTER TABLE aminoblast
	ADD FOREIGN KEY (bjnumber, aanumber)
	REFERENCES amino (bjnumber, aanumber)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE nucleotideblast
	ADD FOREIGN KEY (bjnumber, locustranscript)
	REFERENCES nucleotide (bjnumber, locustranscript)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE aminoblast
	ADD FOREIGN KEY (gi_num)
	REFERENCES refseq (gi_num)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE nucleotideblast
	ADD FOREIGN KEY (gi_num)
	REFERENCES refseq (gi_num)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE amino
	ADD FOREIGN KEY (bjnumber)
	REFERENCES species (bjnumber)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE nucleotide
	ADD FOREIGN KEY (bjnumber)
	REFERENCES species (bjnumber)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;

create index abgi_idx on aminoblast(gi_num);
create index nbgi_idx on nucleotideblast(gi_num);
create index refseq_idx on refseq(refseqid);
create index lower_refseqid_idx on refseq(lower(refseqid));
create index lower_refseqname_idx on refseq(lower(refseq_name));
