import os
import sys

"""
nucleotide sequences:  KEY=<BJNUMBER>,comp0_c0_seq0
  nt/BJ<BJNUMBER>.faa
    >comp0_c0_seq0 ...
    NUCLEOTIDE_SEQUENCE...

peptide sequences:     KEY=<BJNUMBER>,<AANUMBER>
  aa/BJ<BJNUMBER>.faa        WHEN BJNUMBER < 25
    >bj<BJNUMBER>tri|m.<AANUMBER>
    PEPTIDE_SEQUENCE...

  aa/BJ<BJNUMBER>.faa        WHEN BJNUMBER >=25
    >BJ<BJNUMBER>|aa.<AANUMBER>
    PEPTIDE_SEQUENCE...

NT-AA linkage:   KEY=<BJNUMBER>,comp0_c0_seq0,<START>,<END>,<AANUMBER>
  logs/run1/<BJNUMBER>.logfile.txt     WHEN BJNUMBER < 25
    >m.<AANUMBER> ... comp0_c0_seq0:<START>-<END>(<DIRECTION>)
    PEPTIDE_SEQUENCE...

  logs/<BJNUMBER>.logfile.txt          WHEN BJNUMBER >=25
    ><BJNUMBER>|aa.<AANUMBER> ... comp0_c0_seq0:<START>-<END>(<DIRECTION>)


"""


def parse_fasta(filename):
    seqs = {}
    idline = None
    sequence = ''

    for line in open(filename):
        line = line.strip()
        if line == '':
            continue

        if line[0] == '>':
            if idline is not None:
                seqs[idline] = sequence
            idline = line[1:]
            sequence = ''
            continue
        sequence += line

    if idline is not None:
        seqs[idline] = sequence
    return seqs


def parse_nuc(filename):
    """ Handle nucleotide sequences within fasta files.

     filename has the format: 'BJ<BJNUMBER>.fna'
     each record of the fasta file starts with an id followed by a space:
        "comp0_c0_seq0 "...

    returns a dictionary mapping keys to sequences:
        keys are of the format "<BJNUMBER>,'comp0_c0_seq0'"

    """
    bjnumber = filename.replace('.fna', '').split('BJ')[-1]

    nucs = {}
    rawseqs = parse_fasta(filename)
    for idline, seq in rawseqs.iteritems():
        key = idline.split(' ', 2)
        nucs[bjnumber + ",'" + key[0] + "'"] = seq
    return nucs


def parse_aa(filename):
    """ Handle amino acid sequences within fasta files.

     filename has the format: 'BJ<BJNUMBER>.faa'
     each record of the fasta file has 2 possible formats:
        bj<BJNUMBER>tri|m.<AANUMBER>
        BJ<BJNUMBER>|aa.<AANUMBER>

    returns a dictionary mapping keys to sequences:
        keys are of the format '<BJNUMBER>,<AANUMBER>'

    """
    peps = {}
    rawseqs = parse_fasta(filename)
    for idline, seq in rawseqs.iteritems():
        keys = idline.split('|', 2)
        try:
            bjnumber = keys[0].lower().replace('bj', '').replace('tri', '')
            aanumber = keys[1].split('.', 2)[1]
            peps[bjnumber + ',' + aanumber] = seq
        except:
            print idline
    return peps


def parse_logs(filename):
    """

NT-AA linkage:   KEY=<BJNUMBER>,<AANUMBER>,comp0_c0_seq0,<START>,<END>
  logs/run1/<BJNUMBER>.logfile.txt     WHEN BJNUMBER < 25
    >m.<AANUMBER> ... comp0_c0_seq0:<START>-<END>(<DIRECTION>)
    PEPTIDE_SEQUENCE...

  logs/<BJNUMBER>.logfile.txt          WHEN BJNUMBER >=25
    ><BJNUMBER>|aa.<AANUMBER> ... comp0_c0_seq0:<START>-<END>(<DIRECTION>)

    """
    links = set()
    if 'run1' in filename:
        bjnumber = filename.replace('.logfile.txt', '').split('/')[-1]
        bjnumber += ","
        for line in parse_fasta(filename).keys():
            parts = line.split(' ')
            try:
                aakey = bjnumber + parts[0][2:]  # remove 'm.'
                nucinfo = parts[-1]
                nuckey, posinfo = nucinfo.split(':', 2)
                startpos, endpos = posinfo.split('(')[0].split('-', 2)
                links.add(aakey + ",'" + nuckey + "'," + startpos + "," + endpos)
            except:
                print line

    else:
        for line in open(filename):
            parts = line.split("\t", 2)
            try:
                aakey = parts[0].replace('|aa.', ',')
                nucinfo = 'comp' + (parts[1].rsplit(' comp', 2)[-1])
                nuckey, posinfo = nucinfo.split(':', 2)
                startpos, endpos = posinfo.split('(')[0].split('-', 2)
                links.add(aakey + ",'" + nuckey + "'," + startpos + "," + endpos)
            except:
                print line
    return links

if __name__ == "__main__":
    for filename in os.listdir("/lustre/groups/janieslab/4db2/nt"):
        if filename[:2] != 'BJ' or '.fna' not in filename or 'refseq' in filename:
            continue
        print >>sys.stderr, "nt", filename
        for nuc, seq in parse_nuc(os.path.join("/lustre/groups/janieslab/4db2/nt", filename)).iteritems():
            print "INSERT INTO nucleotide (bjnumber,locustranscript,nuc_sequence) VALUES (%s,'%s');" % (nuc, seq)

    for filename in os.listdir("/lustre/groups/janieslab/4db2/aa"):
        if '.faa' not in filename or 'refseq' in filename:
            continue
        print >>sys.stderr, "aa", filename
        for pep, seq in parse_aa(os.path.join("/lustre/groups/janieslab/4db2/aa", filename)).iteritems():
            print "INSERT INTO amino (bjnumber,aanumber,amino_sequence) VALUES (%s,'%s');" % (pep, seq)

    for filename in os.listdir("/lustre/groups/janieslab/4db2/logs"):
        if '.logfile.txt' not in filename or 'refseq' in filename:
            continue
        print >>sys.stderr, "logs", filename
        for linkline in parse_logs(os.path.join("/lustre/groups/janieslab/4db2/logs", filename)):
            print "INSERT INTO nucleotide2amino (bjnumber,aanumber,locustranscript,startpos,endpos) VALUES (%s);" % (linkline,)

    for filename in os.listdir("/lustre/groups/janieslab/4db2/logs/run1"):
        if '.logfile.txt' not in filename or 'refseq' in filename:
            continue
        print >>sys.stderr, "log", filename
        for linkline in parse_logs(os.path.join("/lustre/groups/janieslab/4db2/logs/run1", filename)):
            print "INSERT INTO nucleotide2amino (bjnumber,aanumber,locustranscript,startpos,endpos) VALUES (%s);" % (linkline,)
