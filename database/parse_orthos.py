import sys

"""
  grep '^>' *.fasta | sed 's/-SacBra.faa.outgrouped.fasta.NoACC.Outgroup.fasta:>/\t/' >~/echino_orthoclusters.txt


echino1101-40 bj10@m.43165
echino1101-40 bj34@aa.2903
echino1101-40 bj7@m.87522
echino1101-40 refseqspurp@aa18308
echino1101-40 bj25@aa.28032
echino1101-40 bj34@aa.6774
echino1101-40 bj36@aa.27789

"""

if __name__ == "__main__":
    for line in open(sys.argv[1]):
        clusterid, aainfo = line.strip().split('\t', 2)
        if 'refseq' in aainfo or '@' not in aainfo:
            continue

        bjnumber, aanumber = aainfo.split('@', 2)
        bjnumber = bjnumber[2:]
        aanumber = aanumber.split('.', 2)[-1]

        print "INSERT INTO orthoclusters (clusterid,bjnumber,aanumber) VALUES ('%s',%s,%s);" % (clusterid, bjnumber, aanumber)
