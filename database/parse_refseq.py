import sys


def parse_fasta(filename):
    seqs = {}
    idline = None
    sequence = ''

    for line in open(filename):
        line = line.strip()
        if line == '':
            continue

        if line[0] == '>':
            if idline is not None:
                seqs[idline] = sequence
            idline = line[1:]
            sequence = ''
            continue
        sequence += line

    if idline is not None:
        seqs[idline] = sequence
    return seqs

if __name__ == "__main__":
    SQL = "INSERT INTO refseqs (gi_num,rfname,otherids,amino_sequence) VALUES (%s,E'%s','%s','%s');"
    refseqseqs = parse_fasta("/lustre/groups/janieslab/4db2/aa/refseqSpurp.faa")
    key2gi = {}

    for line in open("/lustre/groups/janieslab/4db2/refseqSpurp.logfile.txt"):
        rfid, tags, refname = line.strip().split('\t')

        if "'" in refname:
            refname = refname.replace("'", "\\'")

        otherids = []
        rtags = {}
        # remove last entry (either blank or symbol)
        tags = tags.split('|')[:-1]
        for i in range(0, len(tags), 2):
            key = tags[i]
            val = tags[i + 1]
            if val == '' or key == '':
                continue
            rtags[key] = val
            otherids.append(key + "=>" + val)

        otherids = ','.join(otherids)

        if rfid not in refseqseqs:
            print >>sys.stderr, rfid, "NOT FOUND"
        else:
            print SQL % (rtags['gi'], refname, otherids, refseqseqs[rfid])
            key2gi[rfid.lower()] = rtags['gi']

    for line in open(sys.argv[1]):
        clusterid, aainfo = line.strip().split('\t', 2)
        if 'refseq' not in aainfo:
            continue

        aainfo = aainfo.lower().replace('@', '|')

        print "UPDATE orthoclusters set gi_num=%s WHERE clusterid='%s';" % (key2gi[aainfo], clusterid)
