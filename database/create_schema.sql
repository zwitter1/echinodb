
/* Drop Tables */

DROP TABLE IF EXISTS nucleotide2amino;
DROP TABLE IF EXISTS aminoblast;
DROP TABLE IF EXISTS amino;
DROP TABLE IF EXISTS nucleotideblast;
DROP TABLE IF EXISTS nucleotide;
DROP TABLE IF EXISTS refseq;
DROP TABLE IF EXISTS species;




/* Create Tables */

CREATE TABLE amino
(
	bjnumber int NOT NULL,
	aanumber int NOT NULL,
	amino_sequence text,
	PRIMARY KEY (bjnumber, aanumber)
) WITHOUT OIDS;


CREATE TABLE aminoblast
(
	bjnumber int NOT NULL,
	aanumber int NOT NULL,
	gi_num varchar NOT NULL,
	evalue decimal,
	alignment_length int,
	PRIMARY KEY (bjnumber, aanumber, gi_num)
) WITHOUT OIDS;


CREATE TABLE nucleotide
(
	bjnumber int NOT NULL,
	locustranscript varchar NOT NULL,
	nuc_sequence text,
	PRIMARY KEY (bjnumber, locustranscript)
) WITHOUT OIDS;

CREATE TABLE nucleotide2amino
(
	bjnumber int NOT NULL,
	locustranscript varchar NOT NULL,
	aanumber int NOT NULL,
	PRIMARY KEY (bjnumber, locustranscript, aanumber)
) WITHOUT OIDS;

CREATE TABLE nucleotideblast
(
	bjnumber int NOT NULL,
	locustranscript varchar NOT NULL,
	gi_num varchar NOT NULL,
	percent_identity decimal,
	alignment_length int,
	mismatches int,
	gapopenings int,
	qstart int,
	qend int,
	sstart int,
	send int,
	evalue decimal,
	score decimal,
	PRIMARY KEY (bjnumber, locustranscript, gi_num)
) WITHOUT OIDS;


CREATE TABLE refseq
(
	gi_num varchar NOT NULL,
	refseqid varchar NOT NULL,
	refseq_name varchar,
	refseq_sequence varchar,
	refseq_type varchar,
	PRIMARY KEY (gi_num)
) WITHOUT OIDS;


CREATE TABLE species
(
	bjnumber int NOT NULL,
	sp_class varchar,
	sp_order varchar,
	sp_family varchar,
	sp_genus varchar,
	sp_species varchar,
	PRIMARY KEY (bjnumber)
) WITHOUT OIDS;

\copy species from 'echino_species.txt'
